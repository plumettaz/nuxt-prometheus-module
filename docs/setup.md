## Setup
- Add `@qonfucius/nuxt-prometheus-module` dependency using yarn or npm to your project
- Add `@qonfucius/nuxt-prometheus-module` to `modules` section of `nuxt.config.js`

```js
{
  modules: [
    // Simple usage
    '@qonfucius/nuxt-prometheus-module',
    
    // With options
    [
        '@qonfucius/nuxt-prometheus-module',
         { 
            port: 9091, 
            host: '0.0.0.0', 
            metrics: {
                collectDefault: true,
                requestDuration: true,
            },
        },
    ],
  ]
}
```
## Demo
Clone `examples` repository.
```bash
$ npm run dev
```