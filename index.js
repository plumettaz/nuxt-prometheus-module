import Prometheus from 'prom-client';
import http from 'http';
import chalk from 'chalk';

module.exports = function PrometheusModule({
  port = 9091,
  host = process.env.HOST || 'localhost',
  metrics: {
    collectDefault = true,
    requestDuration = true,
  } = {},
} = {}) {
  if (collectDefault) {
    Prometheus.register.clear();
    const metricsInterval = Prometheus.collectDefaultMetrics(typeof collectDefault === 'object' ? collectDefault : {});
    process.on('SIGTERM', () => {
      clearInterval(metricsInterval);
    });
  }

  if (requestDuration) {
    const httpRequestDurationMicroseconds = new Prometheus.Histogram({
      name: 'http_request_duration_ms',
      help: 'Duration of HTTP requests in ms',
      labelNames: ['method', 'route', 'code'],
      buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500],
    });

    this.addServerMiddleware(
      (req, res, next) => {
        const startEpoch = Date.now();
        res.once('finish', () => {
          const responseTimeInMs = Date.now() - startEpoch;
          httpRequestDurationMicroseconds
            .labels(req.method, req.originalUrl, res.statusCode)
            .observe(responseTimeInMs);
        });
        next();
      },
    );
  }
  const server = http.createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': Prometheus.register.contentType });
    res.end(Prometheus.register.metrics());
  });
  this.nuxt.hook('ready', (nuxt) => {
    if (nuxt.options.server) {
      server.listen(port, host, () => {
        nuxt.options.cli.badgeMessages.push(chalk.bold('Metrics server: ') + chalk.underline.blue(`http://${host}:${port}/`));
      }).listen(port, host);
    }
  });
  this.nuxt.hook('close', (nuxt) => {
    nuxt._logger.info('Stopping metrics server');
    server.close();
  });
};
