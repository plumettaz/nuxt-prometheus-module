export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'examples',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // Simple usage
      //'@qonfucius/nuxt-prometheus-module',

     // With options
     [
      '@qonfucius/nuxt-prometheus-module',
        { 
          port: 9091,  
          metrics: {
              collectDefault: true,
              requestDuration: false,
          },
        },      
    ],
  ],
}
