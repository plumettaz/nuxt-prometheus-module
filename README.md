# Nuxt Prometheus module

> Use this module to expose metrics to another port (you need to use nuxt start or nuxt dev)
[Documentation](https://qonfucius.gitlab.io/nuxt-prometheus-module/)

## Setup
- Add `@qonfucius/nuxt-prometheus-module` dependency using yarn or npm to your project
- Add `@qonfucius/nuxt-prometheus-module` to `modules` section of `nuxt.config.js`
```js
{
  modules: [
    // Simple usage
    '@qonfucius/nuxt-prometheus-module',
    
    // With options
    [
        '@qonfucius/nuxt-prometheus-module',
         { 
            port: 9091, 
            host: '0.0.0.0', 
            metrics: {
                collectDefault: true,
                requestDuration: true,
            },
        },
    ],
  ]
}
```

## Options

### `port`
Default: `9091`

Port where metrics will be available

### `host`
Default: `127.0.0.1`

Host to bind. Use `0.0.0.0` to be available everywhere, `127.0.0.1` mean "only available on the current host"

### `metrics`

Enable/Disable some metrics

#### `collectDefault`
Default: `true`

Send default metrics about nodejs itself. Pass object to send options to `Prometheus.collectDefaultMetrics`.

#### `requestDuration`
Default: `true`

Send request duration tile with routes.

## Demo
Clone `examples` repository.
```bash
$ npm run dev
```